FROM eclipse-temurin:11.0.18_10-jdk-jammy

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends zip unzip xz-utils git bash curl \
    && echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen \
    && locale-gen en_US.UTF-8 \
    && rm -rf /var/lib/apt/lists/*

COPY flutter /opt/flutter
COPY cmdline-tools /opt/cmdline-tools

RUN /opt/flutter/bin/flutter precache

COPY android-sdk-presigned /opt/android-sdk

ARG PRECACHE_ANDROID_SDK="'build-tools;30.0.3' 'build-tools;33.0.2' 'emulator' 'patcher;v4' 'platform-tools' 'platforms;android-29' 'platforms;android-30' 'platforms;android-31' 'platforms;android-33'"
RUN /opt/cmdline-tools/bin/sdkmanager --sdk_root=/opt/android-sdk --install ${PRECACHE_ANDROID_SDK}

ENV PATH /opt/flutter/bin:/opt/cmdline-tools/bin:$PATH
ENV ANDROID_SDK_ROOT /opt/android-sdk

CMD /usr/bin/bash

