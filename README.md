# Flutter builder base image

Download the pre-built Flutter and Android Command-line tools:

```bash
./prepare.sh
```

Then build the Docker image:

```bash
docker image build -t flutter-builder:latest .
```
