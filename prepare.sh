#/usr/bin/bash

download_flutter() {
  FORCE=$1
  FLUTTER_LOCAL_ARCHIVE="./flutter.tar.xz"
  FLUTTER_LOCAL_EXT_DIR="./flutter"
  FLUTTER_SOURCE="https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_3.7.6-stable.tar.xz"

  if [[ $FORCE -eq 1 || ! -e $FLUTTER_LOCAL_ARCHIVE || ! -e $FLUTTER_LOCAL_EXT_DIR ]]; then
    curl -L -o $FLUTTER_LOCAL_ARCHIVE $FLUTTER_SOURCE
    tar -xvf $FLUTTER_LOCAL_ARCHIVE
  else
    echo "Flutter already exists."
  fi
}

download_android_clitools() {
  FORCE=$1
  ANDROID_CLITOOLS_LOCAL_ARCHIVE="./cmdline-tools.zip"
  ANDROID_CLITOOLS_LOCAL_EXT_DIR="./cmdline-tools"
  ANDROID_CLITOOLS_SOURCE="https://dl.google.com/android/repository/commandlinetools-linux-9477386_latest.zip"

  if [[ $FORCE -eq 1 || ! -e $ANDROID_CLITOOLS_LOCAL_ARCHIVE || ! -e $ANDROID_CLITOOLS_LOCAL_EXT_DIR ]]; then
    curl -L -o $ANDROID_CLITOOLS_LOCAL_ARCHIVE $ANDROID_CLITOOLS_SOURCE
    unzip $ANDROID_CLITOOLS_LOCAL_ARCHIVE
  else
    echo "Android CLI Tools already exists."
  fi
}

download_flutter
download_android_clitools

if [[ ! -e ./android-sdk-presigned ]]; then
  ./cmdline-tools/bin/sdkmanager --sdk_root=./android-sdk-presigned --licenses
fi

